## [2.1.3](https://gitlab.com/onekind/jedi-vue/compare/v2.1.2...v2.1.3) (2022-09-13)


### Bug Fixes

* update icon ([82f0bee](https://gitlab.com/onekind/jedi-vue/commit/82f0bee6fd0503c42527cf9a39a6a25611c00201))

## [2.1.2](https://gitlab.com/onekind/jedi-vue/compare/v2.1.1...v2.1.2) (2022-09-13)


### Bug Fixes

* remove date decoration ([f4221c1](https://gitlab.com/onekind/jedi-vue/commit/f4221c1b7b4170d12f3b328f65b0fd7dae740670))

## [2.1.1](https://gitlab.com/onekind/jedi-vue/compare/v2.1.0...v2.1.1) (2022-09-09)


### Bug Fixes

* introduce Sample component and refactor the structure a bit ([a38fcdc](https://gitlab.com/onekind/jedi-vue/commit/a38fcdc20bffb2f263dc7fe55812d412489ba176))
* remove unecessary dep ([bb71290](https://gitlab.com/onekind/jedi-vue/commit/bb7129045072afe453a110bbb17fc49532795475))

# [2.1.0](https://gitlab.com/onekind/jedi-vue/compare/v2.0.2...v2.1.0) (2022-09-09)


### Bug Fixes

* add ellipsis to the overflow text ([b6ddb84](https://gitlab.com/onekind/jedi-vue/commit/b6ddb84b480b4ed0d921b2ab5e0cbcf8a3f43862))


### Features

* add the startCollapsed feature ([b26a463](https://gitlab.com/onekind/jedi-vue/commit/b26a463caba57a7392483f44b399e248502f24ea))

## [2.0.2](https://gitlab.com/onekind/jedi-vue/compare/v2.0.1...v2.0.2) (2022-09-08)


### Bug Fixes

* add missing documentation and make sprites customizable ([6f1c88b](https://gitlab.com/onekind/jedi-vue/commit/6f1c88b9993a208b6f34c30306ba761b373b8118))

## [2.0.1](https://gitlab.com/onekind/jedi-vue/compare/v2.0.0...v2.0.1) (2022-09-08)


### Bug Fixes

* missing README logo ([06bb1dc](https://gitlab.com/onekind/jedi-vue/commit/06bb1dcfcf8eae659c032101c24631ce457fd528))

# [2.0.0](https://gitlab.com/onekind/jedi-vue/compare/v1.0.2...v2.0.0) (2022-09-08)


### Features

* improve the testbed ([8707fb2](https://gitlab.com/onekind/jedi-vue/commit/8707fb26f6618eacb78aedbee489aa47ad3d4d35))
* vue 3 implementation and streamline ([43e02ce](https://gitlab.com/onekind/jedi-vue/commit/43e02ce7a8d4d529b374179d0798bd2071f90095))


### BREAKING CHANGES

* upgrade to vue 3 and vite

## [1.0.1](https://gitlab.com/onekind/jedi-vue/compare/v1.0.0...v1.0.1) (2022-04-20)


### Bug Fixes

* prevent button from overflowing ([fa8df69](https://gitlab.com/onekind/jedi-vue/commit/fa8df6979f044a8d48b6e54248a6b9a77847aad3))

# 1.0.0 (2022-01-22)


### Features

* new pages demonstration ([1f82c69](https://gitlab.com/onekind/jedi-vue/commit/1f82c690ed1c60b8a65ec6bf58348ab9a0e43d00))

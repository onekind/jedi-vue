# @onekind/jedi-vue

![logo](https://gitlab.com/onekind/jedi-vue/raw/master/docs/public/logo.svg)

[![build status](https://img.shields.io/gitlab/pipeline/onekind/jedi-vue/master.svg?style=for-the-badge)](https://gitlab.com/onekind/jedi-vue.git)
[![npm-publish](https://img.shields.io/npm/dm/@onekind/jedi-vue.svg?style=for-the-badge)](https://www.npmjs.com/package/@onekind/jedi-vue)
[![release](https://img.shields.io/npm/v/@onekind/jedi-vue?label=%40onekind%2Fjedi-vue%40latest&style=for-the-badge)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=for-the-badge)](https://github.com/semantic-release/semantic-release)

A Vue JSON editor

Checkout the [Demo](https://onekind.gitlab.io/jedi-vue/) which contains the component documentation.

> If you enjoy this component, feel free to drop me feedback, either via the repository, or via jose@onekind.io.

## Instalation

```bash
yarn add @onekind/jedi-vue
```

## Setup

### Vue

- Add the following to you application main.js file:

```js
import {Jedi} from '@onekind/jedi-vue'

Vue.use(Jedi)
```

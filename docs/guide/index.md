# Get Started

## Installing the component

```bash
yarn add @onekind/jedi-vue
```

## Importing

### Component import

Add all the components, including the external ones to your main project file.

```js
import JediVue from '@onekind/jedi-vue'

app.use(JediVue)
```

## Options

There are a few options to customize how the component behaves:

- `title`: an optional title string
- `allowEdit`: a boolean flag (`false` by default) to allow the component to edit the model data
- `dark`: dark mode flag (`false` by default)
- `startCollapsed`: `false` by default, allows users to ensure the component starts with only the first line displayed

## Customizing

There are several CSS custom properties which allow configuration of the component, these are:

* `--ok-jedi-vue-color-border-hover`: the color of the border when the user hovers an editable line
* `--ok-jedi-vue-color-dimmed`: the color for pseudo values such as `null`
* `--ok-jedi-vue-color-editor-background`: the color for the input field when `allowEdit` attribute is `true`
* `--ok-jedi-vue-color-type`: the color for the `type` representation (i.e: `{Object}`)
* `--ok-jedi-vue-font-family`: the font family for the main content
* `--ok-jedi-vue-font-size`: the base font size for the component
* `--ok-jedi-vue-font-weight`: the font weight for the main content
* `--ok-jedi-vue-ms-font-family`: the font family for all monospaced elements (i.e.: the `type`)
* `--ok-jedi-vue-ms-font-weight`: the font weight for all monospaced elements
* `--ok-jedi-vue-primary-dark`: the primary color for when the `dark` attribute is set
* `--ok-jedi-vue-primary`: the primary color
* `--ok-jedi-vue-sprite-dark`: the sprite used for the dark version tree decoration
* `--ok-jedi-vue-sprite-light`: the sprite used for the light version tree decoration

> both sprites are available in the `src/assets` folder for customization. they must be converted to `base64` when used.

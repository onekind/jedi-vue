# Usage

## Default

> this example will display test data

<Sample title="Default">
  <JediBasic />
</Sample>

<<< @/examples/JediBasic.vue

## Dark and Collapsed

> different theme for dark mode (manually toggled), also notice it's collapsed by default

<Sample title="Dark and Collapsed">
  <JediDarkAndCollapsed />
</Sample>

<<< @/examples/JediDarkAndCollapsed.vue

## Editable

<Sample title="Editable" column>
  <JediEditable />
</Sample>

<<< @/examples/JediEditable.vue

## Complex

<JediComplex />

<<< @/examples/JediComplex.vue

<script setup>
import JediBasic from '../../examples/JediBasic.vue'
import JediDarkAndCollapsed from '../../examples/JediDarkAndCollapsed.vue'
import JediEditable from '../../examples/JediEditable.vue'
import JediComplex from '../../examples/JediComplex.vue'
</script>

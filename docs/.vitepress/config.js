import packageJson from '../../package.json'
import { resolve, join } from 'path'
import dirTree from 'directory-tree'
import { defineConfig } from 'vitepress'

// NOTE: The order of these pages is also the order
// of appearance in the navigation
const pages = ['documentation']
const documents = dirTree(join(__dirname, '../guide'), { extensions: /\.md/ })

const setLink = (name, path) => {
  const currentPath = resolve(__dirname, '../')
  let link = path.replace(`${currentPath}`, '')
  link = link.replace(/\\/g, '/').replace('.md', '')
  link = `${link}`
  return link
}

const navigation = () => {
  const nav = []
  pages.forEach((page) => {
    const content = documents.children.find(
      ({ name }) => name === page.toLowerCase(),
    )?.children
    nav.push({
      text: page.charAt(0).toUpperCase() + page.slice(1),
      items: content.map(({ name, path }) => {
        return {
          text: name.replace(/-/g, ' ').replace('.md', ''),
          link: setLink(name, path),
        }
      }),
    })
  })
  return nav
}

const sidebar = () => {
  return [
    {
      text: 'General',
      items: [
        { text: 'Get Started', link: '/guide/' },
      ],
    },
    ...navigation(),
  ]
}

export default defineConfig({
  lang: 'en-GB',
  title: ' ',
  description: packageJson.description,
  outDir: '../public',
  base: process.env.CI_PAGES_DOMAIN ? '/jedi-vue/' : '',
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: `${process.env.CI_PAGES_DOMAIN ? '/jedi-vue/' : '/'}favicon.ico`,
      },
    ],
  ],
  themeConfig: {
    repo: 'https://gitlab.com/onekind/jedi-vue',
    docsDir: 'docs',
    editLinks: true,
    logo: '/logo.svg',
    footer: {
      copyright: 'Copyright © 2021-present Onekind',
    },
    nav: [
      { text: 'Home', link: '/', activeMatch: '^/$' },
      { text: 'Guide', link: '/guide/', activeMatch: '^/guide/' },
      {
        text: 'Release Notes',
        link: 'https://gitlab.com/onekind/jedi-vue/-/releases',
      },
      {
        text: 'Source',
        link: 'https://gitlab.com/onekind/jedi-vue.git',
      },
      {
        text: 'More from Onekind',
        link: 'https://gitlab.com/onekind',
      },
    ],
    sidebar: {
      '/guide/': sidebar(),
      '/': sidebar(),
    },
  },
})
